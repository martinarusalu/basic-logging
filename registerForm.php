<?php require("functions.php") ?>
<html>
  <head>
  </head>
  <body>
    <?php if (logged()) : ?>
      <?php header("Location: index.php") ?>
    <?php else : ?>
      <form action="register.php" method="post">
        Username: <input type="text" name="username" />
        Password: <input type="password" name="password" />
        <button type="submit">Register</button>
      </form>
      <a href="index.php">Log in</a>
    <?php endif ?>
  </body>
</html>
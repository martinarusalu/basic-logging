<?php require("functions.php") ?>
<html>
  <head>
  </head>
  <body>
    <?php if (logged()) : ?>
      You are logged in. <a href="logout.php">log out</a>
    <?php else : ?>
      <form action="login.php" method="post">
        Username: <input type="text" name="username" />
        Password: <input type="password" name="password" />
        <button type="submit">Log in</button>
      </form>
      <a href="registerForm.php">Register</a>
    <?php endif ?>
  </body>
</html>
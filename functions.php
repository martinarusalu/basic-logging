<?php
define("HOST","192.168.1.67");
define("USER","test");
define("PASS","test");
define("DB","test");

function getDatabaseConnection () {
	$con = mysqli_connect(HOST, USER, PASS, DB);
	if ($con->connect_error) {
		die("Connection failed: " . $con->connect_error);
	} else {
    return $con;
  }
}

function register ($username, $password) {
	$con = getDatabaseConnection();
   
	$sql = "INSERT INTO users (username, password) VALUES (?, ?);";
	$query = $con->prepare($sql);
	$query->bind_param('ss', $username, $password);
	$query->execute();

	$con->close();
}

function login ($username, $password) {
  startSession();
	$con = getDatabaseConnection();
   
	$sql = "SELECT id, username, password FROM users WHERE username = ? LIMIT 1;";
	$query = $con->prepare($sql);
	$query->bind_param('s', $username);
	$query->execute();
	 
	$result = $query->get_result();
	$row = $result->fetch_assoc();
  
  $con->close();
  
	if (isset($row) && $row["password"] = $password) {
		$_SESSION['id'] = preg_replace("/[^0-9]+/", "", $row['id']);
		$_SESSION['username'] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $row["username"]);
		$_SESSION['password'] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $row["password"]);
		return true;
	}
  
	return false;
}

function logged () {
  startSession();
  if (isset($_SESSION['id'], $_SESSION['username'], $_SESSION['password'])) {
    $id = $_SESSION['id'];
    $username = $_SESSION['username'];
    $password = $_SESSION['password'];
    
		$con = getDatabaseConnection();
    
		$sql = "SELECT password FROM users WHERE id = ? LIMIT 1;";
		$query = $con->prepare($sql);
		$query->bind_param('i', $id);
		$query->execute();
		
		$result = $query->get_result();
		$row = $result->fetch_assoc();
		if (isset($row)) {
			if ($password == $row['password']){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function startSession () {
	$session_name = 'sessiaeg';
	$secure = false;
	$httponly = true;
	$cookieParams = session_get_cookie_params();
	session_set_cookie_params($cookieParams["lifetime"],
		$cookieParams["path"],
		$cookieParams["domain"],
		$secure,
		$httponly);
	session_name($session_name);
	session_start();
	session_regenerate_id(true);
}

function logout () {
		startSession();
		$_SESSION = array();
		$params = session_get_cookie_params();
		setcookie(session_name(),
			'', time() - 42000,
			$params["path"],
			$params["domain"],
			$params["secure"],
			$params["httponly"]);
		session_destroy();
	}


?>